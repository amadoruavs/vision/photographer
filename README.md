# Photographer

Photographer is AmadorUAVs's drone side image capture system.
It uses an onboard CV ML model to find, classify, and crop ODLCs and send results to the ground.

## Running

`python3 -m photographer`.
