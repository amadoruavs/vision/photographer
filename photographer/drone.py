import asyncio
import mavsdk
# from aiostream import stream
from photographer import settings


class Drone:
    def __init__(self, address, *args, **kwargs):
        """
        Create a new drone connection
        :param address: the address of the drone's MAVLink server
        """
        self.drone = mavsdk.System()
        self.address = address
        self.uuid = -1
        self.mission_url = ""

        self.lat = 0
        self.lon = 0
        self.height = 0
        self.gps_lock_acquired = False

    async def connect(self):
        await self.drone.connect(system_address=self.address)

        async for state in self.drone.core.connection_state():
            print("connect")
            if state.is_connected:
                self.uuid = state.uuid
                break

    async def acquire_gps_lock(self):
        async for health in self.drone.telemetry.health():
            print("acquire_gps_lock")
            if health.is_global_position_ok:
                self.gps_lock_acquired = True
                break

    async def get_telemetry(self):
        """
        return telemetry
        """

        async for package in self.drone.telemetry.position():
            self.lat = package.latitude_deg
            self.lon = package.longitude_deg
            self.height = package.absolute_altitude_m

            yield package


async def mainloop(drone: Drone):
    """
    Mainloop for telemetry.
    """
    await drone.connect()
    await drone.acquire_gps_lock()

    async for position in drone.get_telemetry():
        pass
