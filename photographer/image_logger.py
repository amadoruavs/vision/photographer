from PIL import Image
from datetime import datetime

class ImageLogger:

    def __init__(self, image):
        self.image = image

    def save(self):
        Image.fromarray(self.image).save(f"photos/{datetime.now()}.jpg")