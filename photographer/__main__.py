import cv2, time, sys
import threading
import asyncio
from photographer import settings
from photographer.drone import Drone
from photographer.drone import mainloop as drone_mainloop
# from photographer.image_capture import ImageCapture
from photographer.detect import Detector
from photographer.image_capture import ImageCapture
from photographer.server import setup_server, app

# Setup drone telemetry
# Asyncio mainloop has to run in its own thread,
# so it doesn't block OpenCV loop
# TODO: Further refactor, this file is a mess
drone = Drone(settings.DRONE_ADDRESS)

# Run the telemetry loop in another thread
loop = asyncio.get_event_loop()
drone_t = threading.Thread(target=lambda: loop.run_until_complete(drone_mainloop(drone)))
drone_t.start()

# Wait until telemetry lock before starting
while not drone.gps_lock_acquired:
    time.sleep(1)


# Set up YOLOv5 ODLC detector
print("setting up detector")
detector = ImageCapture(drone)
# detector = Detector(drone, "0")
# detector.load_model("weights/yolov5-650.pt")
# detector.load_stream("0")

# Run detector loop in thread
detector_t = threading.Thread(target=lambda: detector.mainloop())
detector_t.start()

# Setup and run the Flask server for control
setup_server(drone, detector)

try:
    app.run(host="0.0.0.0")
except KeyboardInterrupt:
    drone_t.daemon = True
    detector_t.daemon = True
    sys.exit()
