import cv2, time
from photographer import settings
from photographer.drone import Drone
from photographer.classitron_post import upload
from photographer.image_logger import ImageLogger

class ImageCapture:
    # video_dev: int 
    def __init__(self, drone: Drone):
        self.prev_time = 0
        self.video_dev = cv2.VideoCapture('udpsrc port=5600 caps = "application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96" ! rtph264depay ! avdec_h264 ! videoconvert ! appsink', cv2.CAP_GSTREAMER)
        self.drone = drone

    def process_frame(self, frame):
        """
        Process an image frame.
        Resizes the image based on the scale.
        """
        frame = cv2.resize(frame, (int(frame.shape[1] * settings.RESIZE_SCALE),
                                   int(frame.shape[0] * settings.RESIZE_SCALE)))
        upload(settings.IMS_API_URL,
               frame,
               self.drone.lat, self.drone.lon, self.drone.height,
               settings.IMS_MISSION)

        cv2.imshow('image', frame)

    def image_callbacks(self, detect):

        self.process_frame(detect)

        img = ImageLogger(detect)
        img.save()


    def mainloop(self):
        # while True:
        #     # Get the elapsed time, grab frame
        #     time_elapsed = time.time() - self.prev_time
        #     ret, frame = self.video_dev.read()

        #     # we can calculate how often to display frame given x frames/second:
        #     # same thing as 1 frame / 1/x seconds
        #     if time_elapsed >= 1.0 / settings.FRAME_RATE:
        #         print(f"time elapsed since last frame: {time_elapsed}")
        #         self.prev_time = time.time()
        #         self.process_frame(frame)

        # # After the loop release the cap object
        # vid.release()
        # # Destroy all the windows
        # cv2.destroyAllWindows()

        if not self.video_dev.isOpened():
            print('VideoCapture not opened')
            exit(-1)
        
        while True:
            print("read")
            ret, frame = self.video_dev.read()
            print("read2")

            if not ret:
                print('frame empty')
                break

            cv2.imshow('image', frame)

            if cv2.waitKey(1)&0XFF == ord('q'):
                break# so it doesn't block OpenCV loop


            self.video_dev.release()
            cv2.destroyAllWindows()
