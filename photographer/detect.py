"""
Detect bounding boxes from YOLO.
Original source here: https://github.com/ultralytics/yolov5

This has been heavily modified to cut unnecessary bloat and
directly run detection.
"""
import io
import argparse
import time

import cv2
import torch
import torch.backends.cudnn as cudnn
from numpy import random

from models.experimental import attempt_load
from photographer.utils.datasets import LoadStreams, LoadImages
from photographer.utils.general import check_img_size, non_max_suppression, apply_classifier, scale_coords, xyxy2xywh, \
    strip_optimizer, set_logging, increment_path
from photographer.utils.torch_utils import select_device, load_classifier, time_synchronized
from photographer.utils.image_processing import convert_to_lat_lon, cv2_image_to_buf

from photographer import settings
from classitron_client.client import Client
from classitron_client.types import ODLCStandard, Image
from interoperator.odlc import Orientation, Color, Shape

class Detector:
    """
    YOLOv5 based detector for ODLCs.
    Outputs bounding boxes.
    """
    def __init__(self, drone, device_type, *args, **kwargs):
        """
        Initialize instance variables and set a few
        PyTorch options.
        """
        # Set up drone connection
        self.drone = drone

        # Set up video device and GPU/CPU
        self.video_src = None
        self.device_type = device_type
        self.device = select_device(device_type) # gpu
        self.half = self.device.type != "cpu"
        self.model = None
        self.image_size = 480
        self.vid_path = self.vid_writer = None
        self.datastream = None
        cudnn.benchmark = True # Speeds up inference for const image size

        self.classitron_client = Client(settings.IMS_API_URL)
        self.classitron_client.login(settings.IMS_USER, settings.IMS_PASSWORD)

    def load_model(self, weights):
        """
        Load the model.
        """
        self.model = attempt_load(weights, map_location=self.device) # FP32 YOLO model
        self.class_names = self.model.module.names if hasattr(self.model, "module") else self.model.names
        self.image_size = check_img_size(self.image_size, s=self.model.stride.max())
        if self.half:
            self.model.half() # Use the faster half precision model if supported

    def load_stream(self, video_src):
        self.video_src = video_src
        self.datastream = LoadStreams(self.video_src, img_size=self.image_size)

    def process_image(self, image, bounds):
        if self.drone.mission_url == "":
            print("mission url not set, doing nothing")
            return

        image_width, image_height, channels = image.shape

        # If full-image upload is enabled, upload the
        # whole image
        if settings.IMAGE_MODE != "EACH":
            io_buf = cv2_image_to_buf(image)
            image = self.classitron_client.post_image_data(
                Image(self.drone.mission_url,
                      self.drone.lat,
                      self.drone.lon,
                      self.drone.height,
                      image_width,
                      image_height)
            )
        if settings.IMAGE_MODE == "FULL":
            self.classitron_client.upload_image(image.id, io_buf)
            return

        for x,y,w,h, pred in bounds:
            # If full-image upload was not enabled,
            # crop each ODLC and send it individually
            io_buf = cv2_image_to_buf(image[y:y+h, x:x+w])
            lat, lon = convert_to_lat_lon(x + (w // 2), y + (h // 2),
                                          image_width, image_height,
                                          self.drone.lat, self.drone.lon,
                                          self.drone.height)

            if settings.IMAGE_MODE == "EACH":
                image = self.classitron_client.post_image_data(
                    Image(self.drone.mission_url,
                          self.drone.lat,
                          self.drone.lon,
                          self.drone.height,
                          image_width,
                          image_height)
                )
                self.classitron_client.upload_image(image.id, io_buf)

            odlc = self.classitron_client.post_odlc(
                ODLCStandard(
                    parent_image=image.url,
                    autonomous=True,
                    x=0, y=0, width=w, height=h,
                )
            )

            if settings.IMAGE_MODE == "HYBRID":
                self.classitron_client.upload_odlc_image(
                    odlc.id,
                    io_buf
                )
            print(odlc)


    def mainloop(self):
        """
        Run the actual inference on the webcam input.
        This is a mainloop that blocks until killed.

        Most of this code is copied directly from ultralytics/yolov5/detect.py.
        """
        t0 = time.time()
        img = torch.zeros((1, 3, self.image_size, self.image_size),
                          device=self.device)  # init img
        _ = self.model(img.half() if self.half else img) \
            if self.device.type != 'cpu' else None  # run once

        for path, img, im0s, vid_cap in self.datastream:
            img = torch.from_numpy(img).to(self.device)
            img = img.half() if self.half else img.float()  # uint8 to fp16/32
            img /= 255.0  # 0 - 255 to 0.0 - 1.0
            if img.ndimension() == 3:
                img = img.unsqueeze(0)

            # Inference
            # Leave augment off, it messes up the results
            t1 = time_synchronized()
            pred = self.model(img, augment=False)[0]

            # Apply NMS
            confidence_threshold = 0.25
            iou_threshold = 0.45
            pred = non_max_suppression(pred,
                                       confidence_threshold,
                                       iou_threshold,
                                       agnostic=False)

            # Process detections
            for i, det in enumerate(pred):  # detections per image
                im0, frame = im0s[i].copy(), self.datastream.count
                gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
                odlcs = []

                if len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_coords(img.shape[2:], det[:, :4], im0.shape).round()

                    # Print results
                    s = ""
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += f'{n} {self.class_names[int(c)]}s, '  # add to string

                    # Write results
                    for *xyxy, conf, cls in reversed(det):
                        # print(xyxy)
                        x,y,w,h = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                        height, width, channels = im0.shape
                        odlcs.append((int(x*width), int(y*height),
                                      int(w*width), int(h*height),
                                      self.class_names[int(c)]))

                if len(odlcs) > 0:
                    self.process_image(im0, odlcs)
                # Print time (inference + NMS)
                t2 = time_synchronized()
                print(f'Done. ({t2 - t1:.3f}s)')

