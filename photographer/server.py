import flask
from flask import request, Response

server_drone = None
server_detector = None
app = flask.Flask(__name__)

def setup_server(drone, detector):
    global server_drone
    global server_detector

    server_drone = drone
    server_detector = detector


@app.route("/set_mission", methods=["POST"])
def set_mission():
    data = request.get_json()
    server_drone.mission_url = data["mission_url"]
    return ""
