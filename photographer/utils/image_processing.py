import math
import cv2
import time
import io

def convert_to_lat_lon(x: int, y: int,
                       image_width: int, image_height: int,
                       lat: float, lon: float, altitude: float):
    """
    Convert a pixel (x, y) into latitude and longitude.

    Note that in its current form the function assumes up is north
    and that the camera is pointing perfectly flat. This is subject
    to change.

    :param x: x coordinate of the pixel
    :param y: y coordinate of the pixel
    :param pose: tuple in format (camera_rotation_x_axis, camera_rotation_y_axis, image_lat, image_lon, height)
    :return: tuple in format (latitude, longitude)
    """
    # FOV and focal length, in millimeters
    H_FOV_MM = 16.05
    V_FOV_MM = 12.61
    FOCAL_LEN_MM = 35

    # FOV, in degrees
    h_fov_deg = 2 * math.degrees(math.atan2(H_FOV_MM, (2 * FOCAL_LEN_MM)))
    v_fov_deg = 2 * math.degrees(math.atan2(V_FOV_MM, (2 * FOCAL_LEN_MM)))

    # FOV, in meters
    # Note that you can technically combine the above calculation
    # with this calculation
    # but perhaps it's more clear to write them out separately
    h_fov_meter = 2 * math.tan(h_fov_deg / 2) * altitude
    v_fov_meter = 2 * math.tan(v_fov_deg / 2) * altitude

    # Now get the offset in pixels
    h_offset_px = x - (image_width // 2)
    v_offset_px = y - (image_height // 2)

    # Meters per pixel
    h_meters_per_px = image_width / h_fov_meter
    v_meters_per_px = image_height / v_fov_meter

    # Offset in meters, then convert to degrees
    h_offset_meters = h_offset_px * h_meters_per_px
    v_offset_meters = v_offset_px * v_meters_per_px
    offset_lon = h_offset_meters / (111111 * math.cos(lat))
    offset_lat = v_offset_meters / 111111

    # Return lat and lon
    return lat + offset_lat, lon + offset_lon

def cv2_image_to_buf(image):
    """
    Encode a raw OpenCV image into a PNG Python bytes buffer.

    :param image: opencv image array
    """
    success, buf = cv2.imencode(".png", image)
    if not success:
        print("Error encoding image!", success)
        return

    io_buf = io.BytesIO(buf)
    io_buf.name = f"{time.time()}.png"

    return io_buf
