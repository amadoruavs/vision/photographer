"""
Global constants/settings.
"""

# IMS server information
IMS_API_URL = "http://10.0.0.6:8000"
IMS_USER = "suasdev"
IMS_PASSWORD = "AVuav2018"

# A percentage to scale the image represented as a float
RESIZE_SCALE = 1.75

# Frame rate per second of the image to be processed
# float('inf') to use camera's highest possible fps
FRAME_RATE = 1

# Video device to open (/dev/video*)
# Set to 0 for default camera
VIDEO_DEVICE = 0

# MAVSDK Drone address to connect to
# /dev/ttyACM0 is default serial device
DRONE_ADDRESS = "udp://:14550"

# Whether to upload full image and ODLC
# bounds, or crop per-ODLC to save bandwidth
# One of ["FULL", "EACH", "HYBRID"]
# Full sends full image
# Each sends each cropped ODLC as an image
# Hybrid sends cropped ODLC with image, associated to empty image object
IMAGE_MODE = "FULL"
